var results = db.zips.aggregate([
    {$project: {_id: 0, state: 1, zipcode: "$_id"}},
    {$group: {_id: "$state", numberOfZipcode: {$sum: 1}}},
    {$sort: {numberOfZipcode: -1}},
    {$limit: 4}
]);

printjson (results);
